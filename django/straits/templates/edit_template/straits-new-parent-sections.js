
Vue.component( 'straits-new-parent-sections', {

  template: `{% include 'edit_template/straits-new-parent-sections.html' %}`,

  props: {
  },

  data: function(){ return {
    version: null
  }},

  methods: {

    vSelChange: function(){
      const url  = `{% url 'ajax-versions' %}?temp={{ t_id }}&vid=${this.version}`;
      axios.post( url, {} ).then( () => {
        location.reload();
      });
    },

    vBtnClick: function( type, id ){
      const url = `{% url 'ajax-versions' %}?temp={{ t_id }}&type=${type}&id=${id}`;
      axios.post( url, {} ).then( () => {
        location.reload();
      });
    },

  }, // methods

});