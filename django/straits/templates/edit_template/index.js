
Vue.mixin({
  data: function(){
    return {
      apiVersion: parseInt( '{{ cversion.order }}' ),
      baseUrl: '',
      templateId: parseInt( '{{ t_id }}' ),
      // {% if cversion.locked %}
      vpub: false,
      // {% else %}
      vpub: true,
      // {% endif %}
    };
  },
});

var app = new Vue({
  el: '#app',

  data: {
    question: {},
    questionIdx: null,
    section:  {},
    sections: [],
},

  mounted: function(){
    this.getTemplateSections();

    this.$root.$on( 'modal-question', ({ section, question, questionIdx }) => {
      this.section     = section;
      this.question    = question;
      this.questionIdx = questionIdx;
    });

  },

  methods: {

    activateSection: function( section ){
      this.$refs.straitsSectionsComponent.activateSection( section );
    },

    getQuestionAnswers: function( question ){
        question.answers = [];
        const url  = `${this.baseUrl}/api/templates/quest/${question.id}/ans/?version=${this.apiVersion}`;
        axios.get( url ).then( response => {
            question.answers = response.data.sort( (a, b) => a.order - b.order );
        });
    },

    getSectionQuestions: function( section ){
        section.questions = [];
        const url  = `${this.baseUrl}/api/templates/sect/${section.id}/quests/?version=${this.apiVersion}`;
        axios.get( url ).then( response => {
            section.questions = response.data
                .sort( (a, b) => a.order - b.order )
                .map( question => {
                    this.getQuestionAnswers( question );
                    return question;
                });
        });
      },

    getTemplateSections: function(){
      const url  = `${this.baseUrl}/api/templates/temp/${this.templateId}/sects/?version=${this.apiVersion}`;
      axios.get( url ).then( response => {
          this.sections = response.data
              .map( section => {
                  section.active = section.order == 1;
                  if ( section.active ) {
                      this.activeSection = section.id;
                      this.$nextTick( () => this.activateSection( section ) );
                  }
                  this.activateSection( section );
                  this.getSectionQuestions( section );
                  return section;
              })
              .sort( (a, b) => a.order - b.order );
          });
    },

  }, // methods

});
