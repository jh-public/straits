
Vue.component( 'straits-section-question', {

  template: `{% include 'edit_template/straits-section-question.html' %}`,

  props: {
    index: {
      required: true,
      type: Number,
    },
    question: {
      required: true,
      type: Object,
    },
    section: {
      required: true,
      type: Object,
    },
  },

  data: function(){ return {
  }}, // data

  methods: {

    getColor: function( val ){
      switch ( val ){
          case '1':
              return 'green';
          case '0':
              return 'red';
          case 'x':
              return 'orange';
          case 'NA':
              return 'grey';
      }
    },

    editQuestion: function( question, clone ){
      const _question = JSON.parse( JSON.stringify( question ) );
      if( clone ){
        _question.id    = null;
        _question.order = this.section.questions.length + 1;
      }
      this.$root.$emit( 'modal-question', {
        section: this.section,
        question: _question,
        questionIdx: clone? undefined: this.index,
      });
      $( '#modal-question' ).modal( 'show' );
    },

    deleteQuestion: function( question ){
      const todel = confirm( "Are you sure?" );
      if ( todel ){
        this.section.questions.splice( this.index, 1 );
      }
    },

  }, // methods

});