
// {% load static %}

Vue.component( 'straits-sections', {

  template: `{% include 'edit_template/straits-sections.html' %}`,

  props: {
    sections: {
      required: true,
      type: Array,
    }
  },

  data: function(){ return {
    activeSection: null,
    editorEnabled: '',
  }}, // data

  mounted: function(){
      this.$root.$on( 'modal-question-save', ({ section, question, questionIdx }) => {

        section.questions = section.questions || [];

        if ( questionIdx === undefined ){
          section.questions.push( question );
          return;
        }

        section.questions.splice( questionIdx, 1, question );
      });
  },

  methods: {

    activateSection: function( section ){
      this.sections.map( function (q) {
          var status = false;
          if (q.id == section.id) {
              status = true;
          }
          if (q.active != status) {
              q.active = status;
          }
      });
      this.activeSection = section.id;
    },

    addSectionTab: function(){
      this.setAllInactive();
      const url = `/api/templates/sect/`;
      const newSection = {
        name:        "New Section",
        description: "Please Fill in here...",
        order:       this.sections.length + 1,
        template:    this.templateId,
      };
      axios.post( url, newSection ).then( sect => {
          newSection.active    = true;
          newSection.questions = [];
          this.sections.push( sect );
      });
    },

    newQuestion: function( section ){
      const _question = {
            section: section.id,
            order:   ( section.questions || [] ).length + 1,
            answers: [
                {'label': 'Yes',            'order': '1', 'value': '1',  id: 1, },
                {'label': 'No',             'order': '2', 'value': '0',  id: 2, },
                {'label': "I Don't Know",   'order': '3', 'value': 'x',  id: 3, },
                {'label': 'Not Applicable', 'order': '4', 'value': 'NA', id: 4, },
            ],
      };

      this.$root.$emit( 'modal-question', { section, question: _question });
      $( '#modal-question' ).modal( 'show' );
    },

    setAllInactive: function(){
        this.sections.map( section => {
            section.active = false;
        });
    },

    updateQuestion: function( question ) {
      const url = `${this.baseUrl}/api/templates/quest/${question.id}/`;
      axios.put( url, question );
    },

    updateSection: function( section ){
      const url = `${this.baseUrl}/api/templates/sect/${section.id}/`;
      axios.put( url, section );
    },

  }, // methods

});