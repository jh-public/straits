
Vue.component( 'question-modal', {

  template: `{% include 'question-modal/index.html' %}`,

  props: {
    id: {
      type: String,
      required: true,
    },
    section: {
      type: Object,
      required: true,
    },
    question: {
      type: Object,
      required: true,
    },
    questionIdx: {
      required: true,
    },
  },

  data: function(){ return {
      errors: {},
  }},

  methods: {

    addAnswer: function(){
      var toadd = confirm('Add new answer?');
        if ( toadd ){
            const answer = {
                question:  this.question.id,
                label:     'Input your label here...',
                order:     this.question.answers.length + 1,
                value:     'NA',
            }
            this.question.answers.push( answer );
      }
    },

    cancel: function(){
      $( `#${this.id}` ).modal( 'hide' );
    },

    delAnswer: function( index ){
      var todel = confirm('Are you sure?');
      if ( todel ){
          this.question.answers.splice( index, 1 );
      }
    },

    getQuestionAnswers: function(){
      this.question.answers = this.question.answers || [];
      return this.question.answers;
    },

    hitEnter( event ){
    },

    save: function(){
        if ( this.validate() != 0 ){
            this.$forceUpdate();
            return;
        }
        this.$root.$emit( 'modal-question-save', {
            section: this.section,
            question: this.question,
            questionIdx: this.questionIdx,
        });
        $( `#${this.id}` ).modal( 'hide' );
    },


    validate: function( question ){
        let nerrors = 0
        this.errors = {};
        if ( !this.question.q_name ){
            this.errors.q_name = true;
            nerrors++;
        }
        if ( !this.question.d_name ){
            this.errors.d_name = true;
            nerrors++;
        }
        if ( !this.question.action_plan ){
            this.errors.action_plan = true;
            nerrors++;
        }
        if ( !this.question.weightage ){
            this.errors.weightage = true;
            nerrors++;
        }
      return nerrors;
    },

  }, // methods

});