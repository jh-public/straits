# -*- coding: utf-8 -*-
from __future__                         import unicode_literals
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.contrib.contenttypes.models import ContentType
from django.db                          import models


def get_profile_choices():
    return Profile.objects.values_list( 'pk', 'user__username' )

def get_contenttype_company():
    return ContentType.objects.get( model='company' )

def get_contenttype_profile():
    return ContentType.objects.get( model='profile' )


class Company( models.Model ):
    name        = models.CharField( max_length=100, unique=True )
    country     = models.CharField( max_length=100, unique=True )
    testobject  = GenericRelation(
        'TestObject',
        related_query_name='company_objects',
        content_type_field='company_content_type',
        object_id_field='company_object_id',
    )

    def __unicode__( self ):
        return self.name

    class Meta:
        verbose_name_plural = 'Companies'


class Profile( models.Model ):
    user        = models.OneToOneField( 'auth.User', on_delete=models.PROTECT )
    company     = models.ForeignKey( 'Company', on_delete=models.PROTECT )
    testobject  = GenericRelation(
        'TestObject',
        related_query_name='profile_objects',
        content_type_field='profile_content_type',
        object_id_field='profile_object_id',
    )

    def __unicode__( self ):
        return self.user.username


class TestObject( models.Model ):
    profile_content_type    = models.ForeignKey(
        'contenttypes.ContentType',
        on_delete=models.PROTECT,
        related_name='+',
        limit_choices_to={ 'model': 'profile' },
        default=get_contenttype_profile,
    )
    profile_object_id       = models.PositiveIntegerField( verbose_name='Profile' )
    profile_content_object  = GenericForeignKey( 'profile_content_type', 'profile_object_id' )
    company_content_type    = models.ForeignKey(
        'contenttypes.ContentType',
        on_delete=models.PROTECT,
        related_name='+',
        limit_choices_to={ 'model': 'company' },
        default=get_contenttype_company,
    )
    company_object_id       = models.PositiveIntegerField( verbose_name='Company' )
    company_content_object  = GenericForeignKey( 'company_content_type', 'company_object_id' )


    def __unicode__( self ):
        return '%s:%s' % ( self.profile.user.username, self.company.name )

    @property
    def profile( self ):
        return self.profile_content_type.get_object_for_this_type( pk=self.profile_object_id )

    @property
    def company( self ):
        return self.company_content_type.get_object_for_this_type( pk=self.company_object_id )

    class Meta:
        unique_together = [
            'profile_content_type',
            'profile_object_id',
            'company_content_type',
            'company_object_id',
        ]

