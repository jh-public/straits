# -*- coding: utf-8 -*-
from __future__                         import unicode_literals
from django                             import forms
from django.contrib.contenttypes.admin  import GenericTabularInline
from django.contrib                     import admin
from .base                              import LocalDbModelAdmin, get_model_choices
from ..models                           import *


class TestObjectForm( forms.ModelForm ):

    class Meta:
        model   = TestObject
        fields  = [ 'profile_object_id' ]

    def __init__( self, *args, **kwargs ):
        super( TestObjectForm, self ).__init__( *args, **kwargs )
        self.fields['profile_object_id'].widget = forms.Select(
            choices=get_model_choices( Profile, 'user__username' )
        )


class CompanyTestObjectTabularInline( GenericTabularInline ):
    model       = TestObject
    ct_field    = 'company_content_type'
    ct_fk_field = 'company_object_id'
    form        = TestObjectForm


@admin.register( Company )
class CompanyAdmin( LocalDbModelAdmin ):
    list_filter = [ 'profile' ]
    inlines = [ CompanyTestObjectTabularInline ]
