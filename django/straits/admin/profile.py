# -*- coding: utf-8 -*-
from __future__                         import unicode_literals
from django                             import forms
from django.contrib.contenttypes.admin  import GenericTabularInline
from django.contrib                     import admin
from .base                              import LocalDbModelAdmin, get_model_choices
from ..models                           import *

class TestObjectForm( forms.ModelForm ):

    class Meta:
        model   = TestObject
        fields  = [ 'company_object_id' ]

    def __init__( self, *args, **kwargs ):
        super( TestObjectForm, self ).__init__( *args, **kwargs )
        self.fields['company_object_id'].widget = forms.Select(
            choices=get_model_choices( Company, 'name' )
        )


class ProfileTestObjectTabularInline( GenericTabularInline ):
    model       = TestObject
    ct_field    = 'profile_content_type'
    ct_fk_field = 'profile_object_id'
    form        = TestObjectForm


@admin.register( Profile )
class ProfileAdmin( LocalDbModelAdmin ):
    list_filter = [ 'company' ]
    inlines = [ ProfileTestObjectTabularInline ]
