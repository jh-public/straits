# -*- coding: utf-8 -*-
from __future__                         import unicode_literals
from django                             import forms
from django.contrib                     import admin
from .base                              import get_model_choices
from ..models                           import *


class CompanyListFilter( admin.SimpleListFilter ):
    title = 'company'
    parameter_name = 'company'

    def lookups( self, request, model_admin ):
        return get_model_choices( Company, 'name' )

    def queryset( self, request, queryset ):
        val = self.value()
        if val:
            return queryset.filter( company_object_id = val )
        else:
             return queryset


class ProfileListFilter( admin.SimpleListFilter ):
    title = 'profile'
    parameter_name = 'profile'

    def lookups( self, request, model_admin ):
        return get_model_choices( Profile, 'user__username' )

    def queryset( self, request, queryset ):
        val = self.value()
        if val:
            return queryset.filter( profile_object_id = val )
        else:
             return queryset


class TestObjectForm( forms.ModelForm ):

    class Meta:
        model   = TestObject
        fields  = [ 'company_object_id', 'profile_object_id' ]

    def __init__( self, *args, **kwargs ):
        super( TestObjectForm, self ).__init__( *args, **kwargs )
        self.fields['company_object_id'].widget = forms.Select(
            choices=get_model_choices( Company, 'name' )
        )
        self.fields['profile_object_id'].widget = forms.Select(
            choices=get_model_choices( Profile, 'user__username' )
        )


@admin.register( TestObject )
class TestObjectAdmin( admin.ModelAdmin ):
    using        = 'local'
    list_display = [ 'profile', 'company' ]
    list_filter  = [ CompanyListFilter, ProfileListFilter ]
    form         = TestObjectForm

    def save_model( self, request, obj, form, change ):
        obj.save( using='default' )

    def delete_model( self, request, obj ):
        obj.delete( using='default' )

    def get_queryset(self, request):
        return super( TestObjectAdmin, self ).get_queryset( request ).using( 'default' )

    def formfield_for_foreignkey( self, db_field, request, **kwargs ):
        return super( TestObjectAdmin, self ).formfield_for_foreignkey( db_field, request, using=self.using, **kwargs )

    def formfield_for_manytomany(self, db_field, request, **kwargs):
        return super( TestObjectAdmin, self ).formfield_for_manytomany( db_field, request, using=self.using, **kwargs )
