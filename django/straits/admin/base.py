# -*- coding: utf-8 -*-
from __future__                         import unicode_literals
from django.contrib                     import admin


def get_model_choices( model, label_field, empty_choice = [ ('', '' ) ] ):
    return empty_choice + list( model.objects.values_list( 'pk', label_field ) )


class LocalDbModelAdmin( admin.ModelAdmin ):
    using = 'local'

    def save_model( self, request, obj, form, change ):
        obj.save( using=self.using )

    def delete_model( self, request, obj ):
        obj.delete( using=self.using )

    def get_queryset(self, request):
        return super( LocalDbModelAdmin, self ).get_queryset( request ).using( self.using )

    def formfield_for_foreignkey( self, db_field, request, **kwargs ):
        return super( LocalDbModelAdmin, self ).formfield_for_foreignkey( db_field, request, using=self.using, **kwargs )

    def formfield_for_manytomany(self, db_field, request, **kwargs):
        return super( LocalDbModelAdmin, self ).formfield_for_manytomany( db_field, request, using=self.using, **kwargs )
