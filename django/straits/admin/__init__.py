from __future__                         import unicode_literals
from django.contrib.auth.models         import User
from django.contrib.auth.admin          import UserAdmin
from django.contrib                     import admin
from .base                              import LocalDbModelAdmin

from .company                           import CompanyAdmin
from .profile                           import ProfileAdmin
from .testobject                        import TestObjectAdmin

admin.site.unregister( User )
@admin.register( User )
class LocalUserAdmin( LocalDbModelAdmin, UserAdmin ): pass
