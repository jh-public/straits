# -*- coding: utf-8 -*-
from __future__         import unicode_literals
from django.http        import HttpResponse, JsonResponse
from django.shortcuts   import render, redirect
from django.views       import View
import logging
import requests

class Index( View ):

    def get( self, request, t_id=0, *args, **kwargs ):
        cversion = { 'order': 2 }
        if not t_id:
            return redirect( 'index', 6 )
        t_name = 'Test'
        return render( request, 'edit_template/index.html', locals() )


class Api( View ):
    ''' API proxy to workaround CORS '''

    base_url = 'https://www.dataprotectionmgmt.com/api/templates/'
    headers  = {
        'Authorization': 'Token f203975b6a893f933e3a29caf175a5e2a27a4bcc',
    }

    def get( self, request, **kwargs ):
        url = self.get_url( request, **kwargs )
        response = requests.get( url, headers=self.headers )
        return JsonResponse( response.json(), safe=False )


    def get_url( self, request, resource, resource_id=None, sub_resource=None, sub_resource_id=None ):
        url = '%s%s/' % ( self.base_url, resource )

        if resource_id:
            url = '%s%s/' % ( url, resource_id )

            if sub_resource:
                url = '%s%s/' % ( url, sub_resource )
                if sub_resource_id:
                    url = '%s%s/' % ( url, sub_resource_id )

        version = request.GET.get( 'version', 2 )
        url = '%s?version=%s' % ( url, version )
        return url
