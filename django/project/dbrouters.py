from django.conf    import settings
from straits.models import *

class LocalDbRouter:

    def use_local_db( self, model ):

        if settings.DATABASES.get('local'):

            if model._meta.app_label in ['auth', 'admin', 'contenttypes']:
                return 'local'

            if model == Profile:
                return 'local'

            if model == Company:
                return 'local'


    def db_for_read( self, model , **hints ):
        return self.use_local_db( model )


    def db_for_write( self, model , **hints ):
        return self.use_local_db( model )


    def allow_relations( self, model , **hints ):
        return True

