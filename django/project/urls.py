"""project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib   import admin
from straits          import views

urlpatterns_api = [

    url( r'^(?P<resource>.+?)/(?P<resource_id>(\d)+)/(?P<sub_resource>.+?)/(?P<sub_resource_id>(\d)+)/',
        views.Api.as_view(),
        name='api',
    ),
    url( r'^(?P<resource>.+?)/(?P<resource_id>(\d)+)/(?P<sub_resource>.+?)/',
        views.Api.as_view(),
        name='api',
    ),
    url( r'^(?P<resource>temp)/(?P<template_id>(\d)+)/',
        views.Api.as_view(),
        name='api_template',
    ),

]
# /api/templates/sect/${sectionId}/quests/

urlpatterns = [
    url( r'^admin/',            admin.site.urls),

    url( r'^api/templates/',    include( urlpatterns_api ) ),

    url( r'(\d+)/',             views.Index.as_view(), name='index' ),
    url( r'',                   views.Index.as_view(), name='index' ),


    url( r'^all-templates/',    views.Index.as_view(), name='all-templates' ),
    url( r'^ajax-versions/',    views.Index.as_view(), name='ajax-versions' ),
]
