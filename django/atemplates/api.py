from rest_framework import generics, permissions
from rest_framework.response import Response
from rest_framework import status

from versions.functions import version_data

from .models import *
from .serializers import *

#GET THE TEMPLATE
class TemplateDetail(generics.RetrieveAPIView):
	model = Template
	queryset = Template.objects.all()
	serializer_class = TemplateSerializer
	#permission_classes = [
	#	permissions.IsAuthenticated
	#]

#LIST SECTIONS BY TEMPLATE
class TemplateSectionList(generics.ListAPIView):
	model = Section
	queryset = Section.objects.all()
	serializer_class = SectionSerializer

	def get_queryset(self):
		queryset = super(TemplateSectionList, self)\
						.get_queryset()\
						.filter(template__id=self.kwargs.get('pk'))

		tversion = int(self.request.GET.get('version', 1))
		return version_data(tversion, Section, list(queryset))

#CREATE SECTIONS
class SectionCreate(generics.CreateAPIView):
	model = Section
	queryset = Section.objects.all()
	serializer_class = SectionSerializer
	#permission_classes = [
	#	permissions.IsAuthenticated
	#]

#UPDATE SECTIONS
class SectionDetail(generics.RetrieveUpdateAPIView):
	model = Section
	queryset = Section.objects.all()
	serializer_class = SectionSerializer
	#permission_classes = [
	#	permissions.IsAuthenticated
	#]

#LIST QUESTION BY SECTIONS
class SectionQuestionList(generics.ListAPIView):
	model = Question
	queryset = Question.objects.all()
	serializer_class = QuestionSerializer

	def get_queryset(self):
		queryset = super(SectionQuestionList, self)\
						.get_queryset()\
						.filter(section__id=self.kwargs.get('pk'))

		tversion = int(self.request.GET.get('version', 1))
		return version_data(tversion, Question, list(queryset))

#UPDATE QUESTIONS
class QuestionDetail(generics.RetrieveUpdateDestroyAPIView):
	model = Question
	queryset = Question.objects.all()
	serializer_class = QuestionSerializer
	
	#permission_classes = [
	#	permissions.IsAuthenticated
	#]
	def delete(self, *args, **kwargs):
		obj = self.get_object()
		obj.deleted = True
		obj.save()
		serializer = QuestionSerializer(obj)
		return Response(serializer.data)

#LIST ANSWERS BY QUESTIONS
class QuestionAnswerList(generics.ListAPIView):
	model = Answer
	queryset = Answer.objects.all()
	serializer_class = AnswerSerializer

	def get_queryset(self):
		queryset = super(QuestionAnswerList, self)\
						.get_queryset()\
						.filter(question__id=self.kwargs.get('pk'))

		tversion = int(self.request.GET.get('version', 1))
		return version_data(tversion, Answer, list(queryset))

#QUESTION CREATE
class QuestionCreate(generics.CreateAPIView):
	model = Question
	serializer_class = QuestionSerializer
	#permission_classes = [
	#	permissions.IsAuthenticated
	#]

#EDITING ANSWERS
class AnswerDetail(generics.RetrieveUpdateDestroyAPIView):
	model = Answer
	queryset = Answer.objects.all()
	serializer_class = AnswerSerializer
	#permission_classes = [
    #    permissions.IsAuthenticated
    #]

#CREATING ANSWERS
class AnswerCreate(generics.CreateAPIView):
	model = Answer
	serializer_class = AnswerSerializer
	#permission_classes = [
	#	permissions.IsAuthenticated
	#]