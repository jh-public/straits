from rest_framework import serializers

from .models import Template, Section, Question, Answer

class TemplateSerializer(serializers.ModelSerializer):
	class Meta:
		model = Template
		exclude = ('country',)

class SectionSerializer(serializers.ModelSerializer):
	#questions = serializers.HyperlinkedIdentityField('questions',view_name='questions-list')
	class Meta:
		model = Section
		exclude = ('deleted',)

class QuestionSerializer(serializers.ModelSerializer):
	class Meta:
		model = Question
		exclude = ('deleted',)

	def validate(self, data):
		return data

class AnswerSerializer(serializers.ModelSerializer):
	class Meta:
		model = Answer
		exclude = ('deleted',)


