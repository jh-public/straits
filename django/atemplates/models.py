import json
import shortuuid

from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericRelation
from django.contrib.postgres.fields import ArrayField
from django.db import models
from django.db.models.signals import pre_save, post_save
from django.forms.models import model_to_dict
from django.template.defaultfilters import slugify, truncatechars_html
from django.dispatch import receiver

from collections import defaultdict, OrderedDict
from django.contrib.postgres.fields import HStoreField
from redactor.fields import RedactorField

from abstract_templates.models import *
from access_rights.models import Product
from companies.models import Company
from versions.models import VDelta
from softdelete.models import *
from tags.models import NTag, Tag

ATYPE_CHOICES = (
    (1, 'Normal'),
    (2, 'Vendor Outsourcing'),
    (3, 'FreeForm + Parents'),
    (4, 'Logic'),
)

class Template(SoftDeleteObject, AbstractTemplate):
    name = models.CharField("Template Name", max_length=200)
    company = models.ForeignKey(Company, blank=True, null=True, related_name='company_temp')
    owner = models.ForeignKey(User, blank=True, null=True, related_name='owner_template')
    use_ap = models.BooleanField(default=True)
    transferred = models.BooleanField(default=False)
    users = models.ManyToManyField(User)
    
    tags = GenericRelation(NTag, related_query_name='templates')
    inst = models.CharField("Template Name", max_length=200, blank=True, null=True)

    def save(self, *args, **kwargs):
        if not self.id:
            self.uuid = shortuuid.uuid()
        super(Template, self).save(*args, **kwargs)

    @property
    def sc_handle(self):
        return truncatechars_html(self.name, 80)

    @property
    def is_checkout(self):
        return self.users.all() or False    

class TemplateLogic(models.Model):
    template = models.OneToOneField(Template, related_name='tlogic')
    conditions = HStoreField(default={"": {}})
    

    @property
    def pconditions(self):
        cdict = {}
        for k,  v in self.conditions.items():
            cdict[int(k)] = json.loads(v)
        return cdict

class PSection(models.Model):
    name = models.CharField("Parent Section", max_length=50)
    template = models.ForeignKey(Template, related_name='psections')
    order = models.IntegerField(blank=True, null=True)

    class Meta:
        ordering = ['order']


class Section(AbstractSection):
    template = models.ForeignKey(Template, related_name='sections')
    # clonefields
    new_t = models.IntegerField(blank=True, null=True)
    old_s = models.IntegerField(blank=True, null=True)
    psection = models.ForeignKey(PSection, blank=True, null=True)
    versions = GenericRelation(VDelta, content_type_field='ct', object_id_field='oid')
    fcname = models.CharField("FChart Name", max_length=500, blank=True, null=True)

    class Meta:
        ordering = ['order', ]

    @property
    def sc_handle(self):
        return truncatechars_html(self.name, 80)

    @property
    def chart_name(self):
        if self.fcname:
            return self.fcname
        else:
            return self.name
    
    def __init__(self, *args, **kwargs):
        super(Section, self).__init__(*args, **kwargs)
        self.odata = model_to_dict(self)

class Question(AbstractQuestion):
    q_name = models.TextField("Question")
    d_name = models.CharField("Dashboard Name", max_length=400)
    action_plan = models.TextField(blank=True, null=True)
    # if unchecked answer is not included in the dashboard
    use_dashboard = models.BooleanField(default=False)
    # use remarks and actions will have a remarks field
    use_remarks = models.BooleanField(default=False)
    use_files = models.BooleanField(default=True)
    remarks = models.TextField(blank=True, null=True)
    content_align = models.BooleanField(default=False)
    section = models.ForeignKey(Section, related_name='questions')
    # clonefields
    new_s = models.IntegerField(blank=True, null=True)
    old_q = models.IntegerField(blank=True, null=True)
    import_uuid = models.CharField("UUID", max_length=255, blank=True, null=True)
    tlabel = models.CharField("Text Label", max_length=255, default="Remarks")
    ttype = models.BooleanField(default=False)
    versions = GenericRelation(VDelta, content_type_field='ct', object_id_field='oid')
    # Not used as templates has been versioned
    uuid = models.CharField("ShortUUID", max_length=50, blank=True, null=True)
    tags = GenericRelation(NTag, related_query_name='questions')
    
    class Meta:
        ordering = ['order']

    def __unicode__(self):
        return u'%s' % (self.q_name)

    def int_weight(self):
        try:
            return int(self.weightage)
        except:
            return 1

    def __init__(self, *args, **kwargs):
        super(Question, self).__init__(*args, **kwargs)
        self.odata = model_to_dict(self)

    def save(self, *args, **kwargs):
        if not self.id:
            self.uuid = shortuuid.uuid()

        super(Question, self).save(*args, **kwargs)

    @property
    def sc_handle(self):
        return truncatechars_html(self.q_name, 80)
        
class Answer(AbstractAnswer):
    question = models.ForeignKey(Question, related_name='answers')
    versions = GenericRelation(VDelta, content_type_field='ct', object_id_field='oid')
    tags = GenericRelation(NTag)

    class Meta:
        ordering = ['order']

    def __unicode__(self):
        return u"%s %s" % (self.value, self.question_id)

    def __init__(self, *args, **kwargs):
        super(Answer, self).__init__(*args, **kwargs)
        self.odata = model_to_dict(self)

class Version(models.Model):
    name = models.CharField("Version Name", max_length=255)
    temp = models.ForeignKey(Template, related_name='tvers')
    locked = models.BooleanField(default=False)
    date = models.DateField(auto_now_add=True)
    order = models.IntegerField(default=1)

    def __unicode__(self):
        if self.locked:
            return u'Version %s | Created: %s | Published' % (self.order, self.date)
        else:
            return u'Version %s | Created: %s | Draft' % (self.order, self.date)

class Assignment(AbstractAssignment):
    template = models.ForeignKey(Template, related_name='assignment')
    atype = models.CharField("Assignment Type", max_length=50)
    slug = models.SlugField()
    instances = HStoreField(default={})
    inst_track = HStoreField(default={})
    
    vendor_outsourcing = models.BooleanField(default=False)
    assignment_type = models.IntegerField(choices=ATYPE_CHOICES, default=1)
    versions = HStoreField(default={})
    vt = models.ForeignKey('risk_map.VulnTemplate', blank=True, null=True)
    uuid = models.CharField("ShortUUID", max_length=50, null=True)
    

    def __unicode__(self):
        return u'%s %s' % (self.slug, self.company.name)

    def save(self, *args, **kwargs):
        if not self.id:
            self.uuid = shortuuid.uuid()
        super(Assignment, self).save(*args, **kwargs)

    def sorted_instances(self, *args, **kwargs):
        return OrderedDict(sorted(self.instances.items(), key=lambda t: t[0]))

    @property
    def sorted_inst_cont(self, *args, **kwargs):
        inst_dict = OrderedDict()
        for x in self.sorted_instances().values():
            inst_dict[x.replace('-', ' ').upper()] = {}
        return inst_dict

    @property
    def p_inst_track(self, *args, **kwargs):
        vdict = defaultdict(dict)
        for k, v in self.inst_track.items():
            vdict[k] = json.loads(v)
        return vdict

    def c_slug(self, *args, **kwargs):
        return self.sorted_instances().popitem()[1]

    @property
    def dept_var(self):
        return 'assessments-' + self.uuid

class TempCompare(models.Model):
    name = models.CharField("TComp", max_length=200)
    uuid = models.CharField("ShortUUID", max_length=50, null=True)
    secondary = ArrayField(models.IntegerField(blank=True))  # List of primary and secondary template ids

    # COMPARISON/ORGANISING TAGS
    tags = GenericRelation(NTag)  # Store the list of tag libraries for comparison

    def save(self, *args, **kwargs):
        if not self.id:
            self.uuid = shortuuid.uuid()
        super(TempCompare, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.name

@receiver(post_save, sender=Section)
@receiver(post_save, sender=Question)
@receiver(post_save, sender=Answer)
def update_versions(sender, instance, created, *args, **kwargs):
    ct = ContentType.objects.get_for_model(instance)
    vd, created = VDelta.objects.get_or_create(ct=ct, oid=instance.id)

    if created:
        deltas = instance.odata
    else:
        ndata = model_to_dict(instance)
        deltas = {k:v for k, v in ndata.items() if v != instance.odata[k]}
    
    mname = instance.__class__.__name__
    if mname == 'Section':
        vd.deltas[str(instance.template.latest + 1)] = deltas
    elif mname == 'Question':
        vd.deltas[str(instance.section.template.latest + 1)] = deltas
    else:
        vd.deltas[str(instance.question.section.template.latest + 1)] = deltas
    vd.save()