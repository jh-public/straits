from django.db import models
from django.template.defaultfilters import slugify

from collections import OrderedDict
from django_countries.fields import CountryField
from access_rights.models_helpers import AccessObj

from redactor.fields import RedactorField
from access_rights.models import Product
from companies.models import Company

class AbstractTemplate(AccessObj, models.Model):
    description = models.TextField(blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    uuid = models.CharField("ShortUUID", max_length=50, blank=True, null=True)
    latest = models.IntegerField(default=1)    

    def __unicode__(self):
        return u'%s' % (self.name)

    class Meta:
        abstract = True

class AbstractSection(models.Model):
    name = models.CharField("Section Name", max_length=255)
    description = RedactorField(blank=True, null=True)
    order = models.IntegerField()
    deleted = models.BooleanField(default=False)

    def __unicode__(self):
        return u'%s' % (self.name)

    class Meta:
        abstract = True


class AbstractQuestion(models.Model):
    description = models.TextField(blank=True, null=True)
    content = RedactorField(blank=True, null=True)
    weightage = models.CharField("Weightage", max_length=50)
    order = models.IntegerField()
    deleted = models.BooleanField(default=False)
    

    def __unicode__(self):
        return u'%s' % (self.q_name)

    class Meta:
        abstract = True


class AbstractAnswer(models.Model):
    label = models.TextField()
    value = models.CharField("Value", max_length=50)
    order = models.IntegerField()
    deleted = models.BooleanField(default=False)
    
    class Meta:
        ordering = ['order']

    class Meta:
        abstract = True


class AbstractAssignment(models.Model):
    company = models.ForeignKey(Company)
    products = models.ManyToManyField(Product)

    class Meta:
        abstract = True
