import select2

from django.contrib.auth.models import User
from django import forms
from django.forms import Form, RadioSelect, ModelForm, ModelChoiceField, ModelMultipleChoiceField
from django.template.defaultfilters import slugify

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Div, Field, Fieldset, HTML, Layout, Submit
from datetime import date

from access_rights.models import Product, PCAccess
from atemplates.models import Assignment, Template, Version
from companies.models import Company
from users.forms import UserChoiceField


class TemplateForm(ModelForm):
    class Meta:
        model = Template
        exclude = ['company', 'owner', 'transferred', 'users', 'uuid', 'latest', 'country']

    def __init__(self, *args, **kwargs):
        super(TemplateForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.html5_required = True
        self.helper.disable_csrf = False
        self.helper.form_id = 'id-template'
        self.fields['use_ap'].label = 'Enable Action Plan'
        self.helper.layout = Layout(
            Div(
                Div('name', css_class='col-md-9'),
                Div('use_ap', css_class='col-md-3'),
                css_class='row'),
            Div(
                Div('description', css_class='col-md-12'),
                css_class='row'),
        )
        self.helper.add_input(Submit('save', 'Save', css_class="btn-long"))


class TemplateImportForm(ModelForm):
    xlsx = forms.FileField()

    class Meta:
        model = Template
        exclude = ['company', 'owner', 'transferred', 'users', 'latest', 'country']

    def __init__(self, *args, **kwargs):
        super(TemplateImportForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.disable_csrf = False
        self.helper.html5_required = True
        self.helper.form_id = 'id-template'
        self.fields['use_ap'].label = 'Enable Action Plan'
        self.fields['xlsx'].label = 'XLSX'
        self.helper.layout = Layout(
            Div(
                Div('name', css_class='col-md-6'),
                Div('xlsx', css_class='col-md-6'),
                css_class='row'),
            Div(
                Div('use_ap', css_class='col-md-3'),
                css_class='row'),
            Div(
                Div('description', css_class='col-md-12'),
                css_class='row'),
        )
        self.helper.add_input(Submit('save', 'Save', css_class="btn-long"))

    def clean_xlsx(self):
        extension = self.cleaned_data['xlsx'].name.split('.')[1:][0]
        if extension != 'xlsx':

            raise forms.ValidationError(u'Please upload an xlsx file ')
        else:
            pass


class TransferForm(forms.Form):
    email = forms.EmailField()

    def __init__(self, *args, **kwargs):
        super(TransferForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.disable_csrf = False
        self.helper.form_tag = False
        self.helper.html5_required = True
        self.fields['email'].label = ''
        self.helper.layout = Layout(
            Field('email', placeholder="Enter Email"),
        )

    def clean_email(self):
        email = self.cleaned_data.get('email')
        user = User.objects.filter(email=email)
        if email and not user.exists():
            raise forms.ValidationError(u'User is not registed in the system.')
        else:
            # need to check this
            if user[0].profile.alevel != 'Tier1':
                raise forms.ValidationError(u'User needs to be a SuperAdmin of the company to receive the template.')


class AssignmentForm(ModelForm):
    class Meta:
        model = Assignment
        exclude = ['slug', 'instances', 'vendors', 'uuid', 'versions', 'company', 'template', 'inst_track']

    def __init__(self, *args, **kwargs):
        cid = kwargs.pop('cid')
        super(AssignmentForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.disable_csrf = False
        self.helper.html5_required = True
        self.helper.form_id = 'id-assign'
        self.fields['atype'].label = 'Dashboard Name'
        self.fields['vt'].label = 'Vuln Template'
        
        if cid != 1:
            self.fields['companies'] = forms.ModelMultipleChoiceField(required=True,
                                                                      queryset=Company.objects.filter(partners__contains=[cid]))

            pqueryset = Product.objects.filter(
                id__in=PCAccess.objects.filter(company_id=cid).values_list('product_id', flat=True))
            self.fields['products'] = forms.ModelMultipleChoiceField(required=True,
                                                                     queryset=pqueryset)

            self.fields['templates'] = forms.ModelMultipleChoiceField(required=True,
                                                                      queryset=Template.objects.filter(company_id=cid))
        else:
            self.fields['companies'] = forms.ModelMultipleChoiceField(required=True,
                                                                      queryset=Company.objects.all())

            self.fields['products'] = forms.ModelMultipleChoiceField(required=True,
                                                                     queryset=Product.objects.exclude(id=999))
            self.fields['templates'] = forms.ModelMultipleChoiceField(required=True,
                                                                      queryset=Template.objects.exclude(tvers__locked=False))

        self.helper.layout = Layout(
            Div(
                Div('companies', css_class='col-md-12'),
                Div('templates', css_class='col-md-12'),
                Div('atype', css_class='col-md-6'),
                Div('assignment_type', css_class='col-md-6'),
                Div('vt', css_class='col-md-6'),
                Div('products', css_class='col-md-12'),
                css_class='row'),
        )
        self.helper.add_input(Submit('save', 'Save', css_class="btn-long"))

    def save(self, *args, **kwargs):
        companies = self.cleaned_data['companies']
        templates = self.cleaned_data['templates']
        atype = self.cleaned_data['atype']
        products = self.cleaned_data['products']
        at = self.cleaned_data['assignment_type']
        vt = self.cleaned_data['vt']

        for com in companies:
            for template in templates:
                assign, created = Assignment.objects.get_or_create(company=com, template=template)
                assign.atype = atype.upper()
                assign.slug = slugify(atype)
                assign.assignment_type = at
                assign.products = products
                assign.vt = vt
                if self.instance.pk is None and self.instance.assignment_type != 2:
                    inst_str = 'New Instance'
                    assign.instances[inst_str] = slugify(inst_str)
                    assign.versions[slugify(inst_str)] = assign.template.latest
                assign.save()


class BulkAssignmentForm(forms.Form):
    atype = forms.CharField(label="Dashboard Name", )
    template = ModelChoiceField(required=True, queryset=Template.objects.all())
    products = ModelMultipleChoiceField(required=True, queryset=Product.objects.exclude(id=999))
    users = UserChoiceField(queryset=User.objects.all().order_by('id'), required=True)

    def __init__(self, *args, **kwargs):
        super(BulkAssignmentForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.html5_required = True
        self.helper.disable_csrf = False
        self.helper.form_id = 'id-assign'

        self.helper.layout = Layout(
            Div(
                Div('atype', css_class='col-md-6'),
                Div('template', css_class='col-md-6'),
                Div('products', css_class='col-md-12'),
                Div('users', css_class='col-md-12'),
                css_class='row'),
        )
        self.helper.add_input(Submit('save', 'Save', css_class="btn-long"))

    def save(self, *args, **kwargs):
        users = self.cleaned_data['users']
        template = self.cleaned_data['template']
        atype = self.cleaned_data['atype']
        products = self.cleaned_data['products']
        at = self.cleaned_data['assignment_type']

        assign, created = Assignment.objects.get_or_create(company=company, template=template)
        assign.atype = atype.upper()
        assign.slug = slugify(atype)
        assign.assignment_type = at
        assign.products = products

        if self.instance.pk is None and self.instance.assignment_type != 2:
            inst_str = 'New Instance'
            assign.instances[inst_str] = slugify(inst_str)
        assign.save()
