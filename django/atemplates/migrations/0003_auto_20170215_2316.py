# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('atemplates', '0002_auto_20161212_1820'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='template',
            options={'permissions': (('can_undelete', 'Can undelete this object'),)},
        ),
        migrations.AddField(
            model_name='template',
            name='deleted_at',
            field=models.DateTimeField(default=None, null=True, blank=True),
        ),
    ]
