# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('atemplates', '0008_auto_20171101_0803'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='tempcompare',
            name='primary',
        ),
        migrations.RemoveField(
            model_name='tempcompare',
            name='tags',
        ),
    ]
