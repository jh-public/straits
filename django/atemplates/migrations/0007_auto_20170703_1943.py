# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations

def fill_instances(apps, schema_editor):
    Assignment = apps.get_model("atemplates", "Assignment")
    for assign in Assignment.objects.all():
        odict = {'product':{}}
        for prod in assign.products.all():
            for v in assign.instances.values():
                odict['product'].setdefault(prod.id, []).append(v)
        assign.inst_track = odict
        assign.save()


class Migration(migrations.Migration):

    dependencies = [
        ('atemplates', '0006_tempcompare'),
    ]

    operations = [
        migrations.RenameField(
            model_name='assignment',
            old_name='vendors',
            new_name='inst_track',
        ),
        migrations.RunPython(fill_instances),
    ]


