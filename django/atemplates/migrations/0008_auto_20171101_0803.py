# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('atemplates', '0007_auto_20170703_1943'),
    ]

    operations = [
        migrations.AlterField(
            model_name='version',
            name='temp',
            field=models.ForeignKey(related_name='tvers', to='atemplates.Template'),
        ),
    ]
