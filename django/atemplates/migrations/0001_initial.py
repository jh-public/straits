# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import redactor.fields
import datetime
from django.utils.timezone import utc
from django.conf import settings
import django.contrib.postgres.fields


class Migration(migrations.Migration):
    dependencies = [
        ('risk_map', '0001_initial'),
        ('access_rights', '0001_initial'),
        ('companies', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Answer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('label', models.CharField(max_length=200, verbose_name=b'Label')),
                ('value', models.CharField(max_length=50, verbose_name=b'Value')),
                ('order', models.IntegerField()),
            ],
            options={
                'ordering': ['order'],
            },
        ),
        migrations.CreateModel(
            name='Assignment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('company', models.ForeignKey(to='companies.Company')),
            ],
        ),
        migrations.CreateModel(
            name='Question',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('q_name', models.TextField()),
                ('d_name', models.CharField(max_length=200, verbose_name=b'Dashboard Name')),
                ('action_plan', models.TextField(null=True, blank=True)),
                ('description', models.TextField(null=True, blank=True)),
                ('use_dashboard', models.BooleanField(default=False)),
                ('use_remarks', models.BooleanField(default=False)),
                ('remarks', models.TextField(null=True, blank=True)),
                ('content', redactor.fields.RedactorField(null=True, blank=True)),
                ('content_align', models.BooleanField(default=False)),
                ('weightage', models.CharField(max_length=50, verbose_name=b'Weightage')),
                ('order', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Section',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200, verbose_name=b'Section Name')),
                ('description', redactor.fields.RedactorField(null=True, blank=True)),
                ('order', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Template',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200, verbose_name=b'Template Name')),
                ('description', models.TextField(null=True, blank=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('company', models.ForeignKey(related_name=b'company_temp', blank=True, to='companies.Company', null=True)),
                ('owner', models.ForeignKey(related_name=b'owner_template', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('transferred', models.BooleanField(default=False)),
                ('use_ap', models.BooleanField(default=True)),
                ('users', models.ManyToManyField(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AddField(
            model_name='section',
            name='template',
            field=models.ForeignKey(related_name=b'sections', to='atemplates.Template'),
        ),
        migrations.AddField(
            model_name='question',
            name='section',
            field=models.ForeignKey(related_name=b'questions', to='atemplates.Section'),
        ),
        migrations.AddField(
            model_name='assignment',
            name='template',
            field=models.ForeignKey(related_name=b'assignment', to='atemplates.Template'),
        ),
        migrations.AddField(
            model_name='answer',
            name='question',
            field=models.ForeignKey(related_name=b'answers', to='atemplates.Question'),
        ),
        migrations.AddField(
            model_name='assignment',
            name='atype',
            field=models.CharField(default='', max_length=50, verbose_name=b'Assignment Type'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='assignment',
            name='slug',
            field=models.SlugField(default=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='assignment',
            name='instances',
            field=django.contrib.postgres.fields.HStoreField(default={}),
        ),
        migrations.AddField(
            model_name='assignment',
            name='products',
            field=models.ManyToManyField(to=b'access_rights.Product'),
        ),
        migrations.AddField(
            model_name='question',
            name='old_q',
            field=models.IntegerField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='question',
            name='new_s',
            field=models.IntegerField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='section',
            name='old_s',
            field=models.IntegerField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='section',
            name='new_t',
            field=models.IntegerField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='section',
            name='name',
            field=models.TextField(),
        ),
        migrations.AddField(
            model_name='question',
            name='use_files',
            field=models.BooleanField(default=True),
        ),
        migrations.AlterField(
            model_name='answer',
            name='label',
            field=models.CharField(max_length=300, verbose_name=b'Label'),
        ),
        migrations.AlterField(
            model_name='section',
            name='name',
            field=models.CharField(max_length=255, verbose_name=b'Section Name'),
        ),
        migrations.AddField(
            model_name='assignment',
            name='vendor_outsourcing',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='assignment',
            name='vendors',
            field=django.contrib.postgres.fields.HStoreField(default={}),
        ),
        migrations.AlterField(
            model_name='question',
            name='d_name',
            field=models.CharField(max_length=400, verbose_name=b'Dashboard Name'),
        ),
        migrations.AddField(
            model_name='assignment',
            name='assignment_type',
            field=models.IntegerField(default=1, choices=[(1, b'Normal'), (2, b'Vendor Outsourcing'), (3, b'FreeForm + Parents')]),
        ),
        migrations.AddField(
            model_name='question',
            name='import_uuid',
            field=models.CharField(max_length=255, null=True, verbose_name=b'UUID', blank=True),
        ),
        migrations.CreateModel(
            name='PSection',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50, verbose_name=b'Parent Section')),
                ('template', models.ForeignKey(related_name=b'psections', to='atemplates.Template')),
                ('order', models.IntegerField(null=True, blank=True)),
            ],
        ),
        migrations.AddField(
            model_name='section',
            name='psection',
            field=models.ForeignKey(blank=True, to='atemplates.PSection', null=True),
        ),
        migrations.AddField(
            model_name='question',
            name='tlabel',
            field=models.CharField(default=b'Remarks', max_length=255, verbose_name=b'Text Label'),
        ),
        migrations.AddField(
            model_name='question',
            name='ttype',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterModelOptions(
            name='psection',
            options={'ordering': ['order']},
        ),
        migrations.AlterModelOptions(
            name='question',
            options={'ordering': ['order']},
        ),
        migrations.AlterModelOptions(
            name='section',
            options={'ordering': ['order']},
        ),
        migrations.AddField(
            model_name='assignment',
            name='vt',
            field=models.ForeignKey(blank=True, to='risk_map.VulnTemplate', null=True),
        ),
        migrations.AddField(
            model_name='assignment',
            name='uuid',
            field=models.CharField(max_length=50, null=True, verbose_name=b'ShortUUID'),
        ),
        migrations.AddField(
            model_name='template',
            name='uuid',
            field=models.CharField(max_length=50, null=True, verbose_name=b'ShortUUID', blank=True),
        ),
        migrations.AlterField(
            model_name='question',
            name='q_name',
            field=models.TextField(verbose_name=b'Question'),
        ),
        migrations.AlterField(
            model_name='question',
            name='q_name',
            field=models.TextField(verbose_name=b'Question'),
        ),
        migrations.AddField(
            model_name='assignment',
            name='deleted',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='template',
            name='deleted',
            field=models.BooleanField(default=False),
        ),
        migrations.CreateModel(
            name='Version',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name=b'Version Name')),
                ('locked', models.BooleanField(default=False)),
                ('temp', models.ForeignKey(to='atemplates.Template')),
                ('date', models.DateField(default=datetime.datetime(2016, 1, 25, 12, 42, 50, 215940, tzinfo=utc), auto_now_add=True)),
                ('order', models.IntegerField(default=1)),
            ],
        ),
        migrations.AddField(
            model_name='template',
            name='latest',
            field=models.IntegerField(default=1),
        ),
        migrations.AddField(
            model_name='answer',
            name='deleted',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='question',
            name='deleted',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='section',
            name='deleted',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='question',
            name='uuid',
            field=models.CharField(max_length=50, null=True, verbose_name=b'ShortUUID', blank=True),
        ),
        migrations.AddField(
            model_name='assignment',
            name='versions',
            field=django.contrib.postgres.fields.HStoreField(default={}),
        ),
    ]
