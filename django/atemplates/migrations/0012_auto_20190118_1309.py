# -*- coding: utf-8 -*-
# Generated by Django 1.11.17 on 2019-01-18 05:09
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('atemplates', '0011_auto_20190108_0919'),
    ]

    operations = [
        migrations.AlterField(
            model_name='answer',
            name='label',
            field=models.TextField(),
        ),
    ]
