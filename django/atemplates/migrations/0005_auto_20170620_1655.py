# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('atemplates', '0004_templatelogic'),
    ]

    operations = [
        migrations.AlterField(
            model_name='assignment',
            name='assignment_type',
            field=models.IntegerField(default=1, choices=[(1, b'Normal'), (2, b'Vendor Outsourcing'), (3, b'FreeForm + Parents'), (4, b'Logic')]),
        ),
        migrations.AlterField(
            model_name='templatelogic',
            name='template',
            field=models.OneToOneField(related_name='tlogic', to='atemplates.Template'),
        ),
    ]
