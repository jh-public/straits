# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.contrib.postgres.fields


class Migration(migrations.Migration):

    dependencies = [
        ('atemplates', '0003_auto_20170215_2316'),
    ]

    operations = [
        migrations.CreateModel(
            name='TemplateLogic',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('conditions', django.contrib.postgres.fields.HStoreField(default={b'': {}})),
                ('template', models.OneToOneField(to='atemplates.Template')),
            ],
        ),
    ]
