# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.contrib.postgres.fields


class Migration(migrations.Migration):

    dependencies = [
        ('tags', '0007_ntag'),
        ('atemplates', '0005_auto_20170620_1655'),
    ]

    operations = [
        migrations.CreateModel(
            name='TempCompare',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200, verbose_name=b'TComp')),
                ('secondary', django.contrib.postgres.fields.ArrayField(base_field=models.IntegerField(blank=True), size=None)),
                ('uuid', models.CharField(max_length=50, null=True, verbose_name=b'ShortUUID')),
                ('primary', models.ForeignKey(to='atemplates.Template')),
                ('tags', models.ManyToManyField(to='tags.Tag')),
            ],
        ),
    ]
