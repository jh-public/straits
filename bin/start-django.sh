#!/usr/bin/env bash

server_type=django
while getopts 's:' opt; do
    case $opt in
        s) server_type=$OPTARG
        break;;
    esac
done
shift $((OPTIND-1))

mysql_isready(){
    mysql --defaults-file=/app/mysql.cnf -e 'show databases;' 2>/dev/null \
    | grep 'django'
}

wait_for_db(){
    while ! pg_isready -h db; do
        sleep 1
    done

    while ! pg_isready -h dblocal; do
        sleep 1
    done
}

create_default_user(){
./manage.py shell<<EOD
from django.contrib.auth.models import User
try:
    User.objects.get( username='admin' )
except User.DoesNotExist as e:
    User.objects.create_superuser( 'admin', '', 'xxx12345' )
EOD
}

django-server(){
    ./manage.py runserver 0.0.0.0:8000
}

plus-server(){ # django-extensions
    ./manage.py runserver_plus 0.0.0.0:8000
}

gunicorn-server(){
    gunicorn \
        -b 0.0.0.0:8000 \
        --error-logfile=- \
        --access-logfile=- \
        django_project.wsgi
        # --max-requests 1 \
}

daphne-server(){
    daphne -b 0.0.0.0 -p 8000 project.asgi:application
}

main(){
    cd /app/django
    wait_for_db
    ./manage.py migrate --database=local
    ./manage.py migrate
    ./manage.py collectstatic --noinput \
        -i media \
        -i app   \
        -i ae
    create_default_user
    [ -e ./loaddata.sh ] && ./loaddata.sh
    ${server_type}-server $*
}
main "$@"
